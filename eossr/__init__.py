from pathlib import Path

from .version import __version__  # noqa

ROOT_DIR = Path(__file__).absolute().parents[1]
