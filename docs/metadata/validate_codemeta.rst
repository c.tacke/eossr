======================
Validate CodeMeta file
======================

We offer two ways to validate your codemeta file before making a request to add your record into the eOSSR:

1. An `Online Tool <https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.in2p3.fr%2Fescape2020%2Fwp3%2Feossr/master?urlpath=voila%2Frender%2F%2Fdocs%2Fmetadata%2Fvalidate_codemeta.ipynb>`__ using mybinder
2. The command line interface after installing the `eossr` package locally:

.. code-block::

    eossr-metadata-validator codemeta.json
