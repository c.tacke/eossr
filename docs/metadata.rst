OSSR metadata
=============


.. toctree::
   :maxdepth: 2
   :glob:

   metadata/metadata.rst
   metadata/ossr_metadata.ipynb
   metadata/validate_codemeta.rst
   metadata/funder_codemeta.rst
   metadata/Harvest_zenodo.ipynb
